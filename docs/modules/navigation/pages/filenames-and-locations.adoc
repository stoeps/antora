= Navigation Filenames and Locations
:description: An overview of the purpose of an Antora navigation source file, its file format, its naming parameters, and storage location patterns.
:keywords: AsciiDoc navigation file, nav.adoc, repository organization, save navigation in a repository, navigation file best practices
// Filters
:page-collections: core concepts
:page-tags: docs component

On this page, you'll learn:

* [x] The ways visitors can navigate to site pages.
* [x] The purpose of a navigation file.
* [x] The navigation file's extension and naming rules.
* [x] Common and alternate navigation file storage locations.

== Navigation methods

All of the AsciiDoc files in a [.path]_pages_ directory are automatically published to your site by Antora.
That means visitors can find a page using search tools, links on other site pages, and in some scenarios, through dropdown menus such as the page version selector.
When you want visitors to be able to locate and go to a page using a component version page menu, then you must add a cross reference (`xref`) to that page in the appropriate navigation source file.

== What's a navigation source file?

A navigation source file is a list of page cross references, external URLs, and other content that is marked up in AsciiDoc and stored at the base of a xref:ROOT:module-directories.adoc[module directory].
If registered in a component version descriptor ([.path]_antora.yml_), the contents of the navigation file will be incorporated into a component version page menu and published to a site.

== Filename and format

Navigation files must end with the AsciiDoc file extension (`.adoc`).
They're typically named [.path]_nav.adoc_; however, you can use any name that is meaningful to you.
Name your navigation files [.path]_kaboom.adoc_ if that's what makes your team happy.

[#storage]
== File location

A common pattern is for each module to contain its own navigation file.
xref:ROOT:module-directories.adoc#module[Modules already represent logical groups of concepts or objectives], so it's likely that you'd arrange the xrefs to that module's pages into a continuous, hierarchical flow of navigation menu entries.
This also reduces the number of coordinates the page IDs in the xrefs require when you add a page to the navigation file.

Navigation files should be stored at the base of a module directory, i.e., at the same level as a [.path]_pages_ family directory.
*Don't* save navigation files inside [.path]_pages_, otherwise the navigation files will be converted to individual pages and published as pages.

=== Multiple files per module

A module can contain more than one navigation file.
This functionality helps you finely tune the navigation list order when you xref:register-navigation-files.adoc[register the navigation files] in an [.path]_antora.yml_ file.

=== Alternative storage patterns

Storing a navigation file in the module it references simplifies the page cross references entered into it.
It also helps writers locate the navigation that's affected when they add pages to or remove pages from a module.
However, xrefs to other modules' pages (and even other component versions' pages) can be entered into any module's navigation file.
You just need to xref:page:module-and-page-xrefs.adoc#xref-page-across-modules[add the module coordinate] (or xref:page:version-and-component-xrefs.adoc[component name and version coordinates]) to the cross references you create in the navigation file.
For example, you could store a single navigation file in a component version's ROOT module, and, instead of having navigation files in each module in the component version, you could just add the cross references to pages in those other modules directly to the ROOT module's navigation file.
////
== What's next?

Learn how to register navigation files so they get included in a component version page menu:

* xref:register-navigation-files.adoc[Register a component version's navigation files for display in its component version page menu]

Learn all about creating navigation lists and navigation items:

* xref:list-structures.adoc[Navigation lists: list title and item hierarchy, single list file, multi-list file]
* xref:link-syntax-and-content.adoc[Navigation link syntax and content formatting: xrefs, URLs, text styles, images, and more]
////
//A component menu is created when, at runtime, Antora combines one or more navigation files as instructed by a component descriptor file, converts the assembled navigation lists into HTML, wraps the HTML with a UI template, and publishes the resulting component navigation menus to your site.
//A component navigation menu allows site visitors to discover and navigate between a component's pages.
//Antora allows for a variety of use cases so that you can create, store, and assemble the navigation source files to suit your documentation requirements.
//The pages in the Site Navigation category describe the fundamentals of creating and storing navigation files in a documentation component.
// source nesting depth versus published nesting depth, titled versus non-titled lists
