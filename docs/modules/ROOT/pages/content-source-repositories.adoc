= Repositories and Content Source Roots

Antora collects content source files from directories in branches, tags, and worktrees of git repositories.
These source files are only retrieved and processed correctly if Antora can locate them within a content source root.

[#git-and-content-sources]
== git repositories and content sources

The source files containing a site's content are stored in one or more git repositories.
These repositories can be remote, local, or a combination of both.
In a playbook file, you'll enter several parameters (url, git references, and start paths) that 1. allow Antora to locate and connect to your content source repositories, and 2. provide Antora with the location criteria that resolves to one or more <<content-source-root,content source roots>> within a git repository.
//A [.term]*content source* is one or more routes--git references and start paths--that lead to unique <<content-source-root,content source roots>>.
//Content sources are specified in an Antora playbook file.

Before configuring a site's playbook, you'll want to determine where to set up the content source roots in your repositories.
// to be located and then set up the directories Antora requires.
//to place your content source roots to beet up the required directories and sorted your content source files into them at each content source root.

[#content-source-root]
== What's a content source root?

A [.term]*content source root* is a location in a repository from where Antora begins collecting content source files.
Think of the content source root as the entrance of the store.
From a content source root, Antora first looks for an [.path]_antora.yml_ file, then a collection of source files that are organized under a xref:standard-directories.adoc[standard set of directories].

A repository can host a multitude of content source roots, whether located in different branches or tags, in different directories within the same branch or tag, or a combination of both.
